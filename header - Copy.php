
<html>
	<div class="py-1 bg-danger">
    <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
            <div class="col-lg-12 d-block">
                <div class="row d-flex">
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
                        <span class="text">+ 1235 2355 98</span>
                    </div>
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
                        <span class="text">youremail@email.com</span>
                    </div>
                    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
                        <span class="text">3-5 Business days delivery &amp; Free Returns</span>
                    </div>
                </div>
            </div>
        </div>
      </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="index.html"><img src="images/logo.png"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active"><a href="index.php" class="nav-link">Home</a></li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop</a>
          <div class="dropdown-menu" aria-labelledby="dropdown04">
              <a class="dropdown-item" href="shop.php">Shop</a>
              <!-- <a class="dropdown-item" href="wishlist.html">Wishlist</a> -->
            <a class="dropdown-item" href="searchbranch.php">Branches</a>
            <a class="dropdown-item" href="cart.html">Cart</a>
            <a class="dropdown-item" href="checkout.html">Checkout</a>
          </div>
        </li>
          <li class="nav-item"><a href="about.html" class="nav-link">About</a></li>
          <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>
          <li class="nav-item"><a href="<?php if(isset($_SESSION['Users'])){echo("MyProfile.php"); } else echo("login.php");?>" class="nav-link"><?php if(isset($_SESSION['Users'])){echo($_SESSION['Users']); } else   echo("Login");?></a></li>
          <?php if(isset($_SESSION['Users'])){echo('<li class="nav-item"><a href="logout.php" class="nav-link">Logout</a></li>'); } else echo("");?> 
          <li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link"> <span class="icon-shopping_cart"></span><span id='p_number'></span></a></li>
          
          
          <!-- <li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link"> -->
          <?php
          // include_once "Database.php";
          // $db=new Database();
          // $r=$db->RUNSearch("select count(*) from cardorders where username='".$_SESSION['UserCart']."'");
          //   if($row=mysqli_fetch_assoc($r)){
?>
          
          <!-- <span class="icon-shopping_cart"></span>[<?php 
          // echo($row['count(*)']);
          ?>]</a></li> -->
          <?php 
          // }
        ?>
        </ul>
      </div>
    </div>
  </nav>
  <br>
  <!-- </html>
<script> 
// Get the container element
var btnContainer = document.getElementById("ftco-navbar");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("nav-item");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
</script> -->