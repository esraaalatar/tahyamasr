<?php 
session_start();
?>
<!DOCTYPE html>
<?php 
if(isset($_POST['btnsearch']))
{
 $category=$_POST['category'];
$city=$_POST['city'];
header("location:searchproduct.php?catno=".$category."&city=".$city);
//echo("<script> window.open('searchproduct.php?catno='.$category.'and city='.$city,'_parent'); </script>");
}
?>
<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
	<?php 
include_once "header.php";
	?>
    <!-- END nav -->

<br>
    <section class="ftco-section">
	<form method="post" class="form"> 
			<div class="container">
				<div class="row no-gutters ftco-services">
				<h1> Find Your Product here </h1>
				</div> 
				<div class="row no-gutters ftco-services">
          <div class="col-md-12 text-center d-flex align-self-stretch ftco-animate ">
          <select name="category" class="form-control" > 
          <?php
                include_once "Category.php";  
                $dept=new Category();
                $drows=$dept->GetAllCategories();
                while($rs=mysqli_fetch_assoc($drows))
                {
              ?>
          <option>
          <?php echo($rs['category_name']); ?>
           </option> <?php } ?>
                </select>
                
                  <select name="city" class="form-control" > 
                  <?php
                  include_once "Database.php";
                  $db=new Database();
                  $rows=$db->RUNSearch("select * from cities");
                  while($rss=mysqli_fetch_assoc($rows))
                        {
                      ?>
                  <option>
                  <?php echo($rss['city']); ?>
                   </option> <?php } ?>
                        </select>
                  
				 <input type="submit" class=" btn btn-danger btn-lg form-control" value="Find" name="btnsearch">
				 </div>
            </div>    
          </div>
		  </form>
		</section>

		<section class="ftco-section ftco-category ftco-no-pt">
			<div class="container">
							<div class="row justify-content-around flex-wrap ">
              <?php
                include_once "Category.php";  
                $dept=new Category();
                $drows=$dept->GetAllCategories();
                while($rs=mysqli_fetch_assoc($drows))
                {
              ?>
								<div class="category-wrap ftco-animate img mb-2 d-flex align-items-end col-md-4"  style="background-image: url(images/category-<?php echo($rs['category_id']); ?>.jpg);"  href="searchcategoryproducts.php?catno=<?php echo($rs['category_name']); ?>">
									<div class="text px-3 py-1">
										<h2 class="mb-0"><a href="searchcategoryproducts.php?catno=<?php echo($rs['category_name']); ?>"><?php echo($rs['category_name']); ?></a></h2>
									</div>
								</div>
                <?php  }?>
                </div>
                
							</div>
		</section>
<?php
    include_once "footer.php";
		?> 
<!-- footer end -->
  
  </body>
</html>