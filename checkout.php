<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
  <?php 
	include_once "header.php";
		?>  
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Checkout</span></p>
            <h1 class="mb-0 bread">Checkout</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
		<?php if( $_SESSION['CartCount']!=0) { ?>
          <div class="col-xl-7 ftco-animate">
						<form method="post" class="billing-form">
							<h3 class="mb-4 billing-heading">Shipping Details</h3>
	          	<div class="row align-items-end">
				  <?php
				  include_once "Customers.php";
      $users=new Customers();
	  $users->setUsername($_SESSION['Users']); 
	  $users->setCustomerPhone($_SESSION['Users']);
	  $users->setCustomerEmail($_SESSION['Users']);
	  $r=$users->CheckUser();
      if($rows=mysqli_fetch_assoc($r))
           {
          // echo($rows);
               ?>
	          		<div class="col-md-6">
	                <div class="form-group">
					<label for="firstname">Full Name</label>
					<input type="text" class="form-control" name="txtname" value="<?php echo $rows['customer_name']; ?>" required >
	                </div>
	              </div>
                <!-- <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
		            		<label for="country">State / Country</label>
		            		<div class="select-wrap">
		                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
		                  <select name="" id="" class="form-control">
		                  	<option value="">France</option>
		                    <option value="">Italy</option>
		                    <option value="">Philippines</option>
		                    <option value="">South Korea</option>
		                    <option value="">Hongkong</option>
		                    <option value="">Japan</option>
		                  </select>
		                </div>
		            	</div>
		            </div> -->
					<div class="w-100"></div>
				  <div class="col-md-6">
	                <div class="form-group">
	                	<label for="phone">Phone*</label>
	                  <input type="text" class="form-control" name="txtphone" value="<?php echo $rows['customer_phone']; ?>" required >
	                </div>
				  </div>
				  
				  <div class="col-md-6">
	                <div class="form-group">
	                	<label for="landphone">Land Phone</label>
	                  <input type="text" class="form-control" value="<?php echo $rows['customer_land_phone']; ?>" name="landphone" >
	                </div>
	              </div>
                <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
		            		<label for="country">Town / City</label>
		            		<input type="text" class="form-control" value="<?php echo $rows['city']; ?>" name="city" required>
		            	</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="streetaddress">Area</label>
	                  <input type="text" class="form-control"  value="<?php echo $rows['area']; ?>" name="area" required>
	                </div>
		            </div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="towncity">Street</label>
	                  <input type="text" class="form-control" name="street"  value="<?php echo $rows['street']; ?>" required>
	                </div>
		            </div>
		   <?php } ?>
					</div>
	         <!-- END -->
					</div>
					<div class="col-xl-5">
	          <div class="row mt-5 pt-3">
			  <?php
  include_once "Database.php";
  $db=new Database();
  $r=$db->RUNSearch("select sum(subtotal) from cardorders where username='".$_SESSION['UserCart']."'");
	if($row=mysqli_fetch_assoc($r)){
 ?>
	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3 class="billing-heading mb-4">Cart Total</h3>
	          			<p class="d-flex">
		    						<span>Subtotal</span>
		    						<span>$<?php echo($row['sum(subtotal)']); ?></span>
		    					</p>
								<p class="d-flex">
    						<span>Delivery</span>
							<?php $delivery=30.00;?>
    						<span>$	<?php echo($delivery);?>.00</span> 
    					</p>
		    					<p class="d-flex">
		    						<span>Discount</span>
		    						<span>$0.00</span>
		    					</p>
		    					<hr>
		    					<p class="d-flex total-price">
		    						<span>Total</span>
		    						<span>$<?php echo($row['sum(subtotal)']+$delivery); ?>.00</span>
		    					</p>
								
								<div class="form-group">
										<div class="col-md-12">
											<div class="checkbox">
											   <label><input type="checkbox" value="" class="mr-2" checked required> I have read and accept the terms and conditions</label>
											</div>
										</div>
									</div>
									<p> <input type="submit" class="btn btn-primary py-3 px-4" value='Place an order' name="btnorder"></p>
								</div>
								<?php
						
								if(isset($_POST['btnorder']))
								{
									$db=new Database();
									$date=new DateTime("now", new DateTimeZone('Africa/Cairo'));
									$cd=$date->format('Y-m-d H:i:s');
									$total=$row['sum(subtotal)']+$delivery;
									$subtotal=$row['sum(subtotal)'];
									$db->RUNDML("insert into `orders`( `order_date`, `order_status`, `username`, `subtotal`, `delivary`, `total`, `city`, `name`, `phone`, `area`, `street`) VALUES ('".$cd."','pending','".$_SESSION['UserCart']."','".$subtotal."','".$delivery."','".$total."','".$_POST['city']."','".$_POST['txtname']."','".$_POST['txtphone']."','".$_POST['area']."','".$_POST['street']."')","added");
							//////////////////////////////////////
									$r=$db->RUNSearch("select max(order_id) from orders where username='".$_SESSION['UserCart']."'");
									if($row=mysqli_fetch_assoc($r)){
										$max=$row['max(order_id)'];
									}
								$rd=$db->RUNSearch("select * from cardorders where username='".$_SESSION['UserCart']."'");
								while($row=mysqli_fetch_assoc($rd)){
									$db->RUNDML("insert into `order_details` VALUES ('".$max."','".$row['product_id']."','".$row['quantity']."','".$row['price']."','".$row['subtotal']."')","");
							}
						
						$db->RUNDML("delete from add_product where username = '".$_SESSION['UserCart']."'","");		
						$r=$db->RUNSearch("select * from delivery_captians where city='".$_POST['city']."' and status='available'");
						$deliveryid=0;
						if($row=mysqli_fetch_assoc($r)){
							$deliveryid=$row['id'];
						}
						$db->RUNDML("update orders set delivery_id ='".$deliveryid."' where order_id='".$max."'","");
						$db->RUNDML("update delivery_captians set status='busy' where id='".$deliveryid."'","");
					echo("<script type='text/javascript'>alert('order has been sent');</script>");
				}
					
						?>
						<?php
					}
					?>
	          	</div>
	          	<!-- <div class="col-md-12">
	          		<div class="cart-detail p-3 p-md-4">
	          			<h3 class="billing-heading mb-4">Payment Method</h3>
									<div class="form-group">
										<div class="col-md-12">
											<div class="radio">
											   <label><input type="radio" name="optradio" class="mr-2"> Direct Bank Tranfer</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<div class="radio">
											   <label><input type="radio" name="optradio" class="mr-2"> Check Payment</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<div class="radio">
											   <label><input type="radio" name="optradio" class="mr-2"> Paypal</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12">
											<div class="checkbox">
											   <label><input type="checkbox" value="" class="mr-2"> I have read and accept the terms and conditions</label>
											</div>
										</div>
									</div>
									<p><a href="#"class="btn btn-primary py-3 px-4">Place an order</a></p>
								</div> -->
	          	</div>
	          </div>
          </div> <!-- .col-md-8 -->	<?php 
											} else 
											{
												echo('<h3> there is no items in your cart <a href="shop.php"> shop now >> </a>  </h3> ');
											
											}
						   ?>
        </div>
	
      </div>
	  </form>
    </section> <!-- .section -->

		<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
      <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
          <div class="col-md-6">
          	<h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
          	<span>Get e-mail updates about our latest shops and special offers</span>
          </div>
          <div class="col-md-6 d-flex align-items-center">
            <form action="#" class="subscribe-form">
              <div class="form-group d-flex">
                <input type="text" class="form-control" placeholder="Enter email address">
                <input type="submit" value="Subscribe" class="submit px-3">
              </div>
            </form>
          </div>
        </div>
      </div>
	  </form>
    </section>
	<?php 
	
	include_once "footer.php";
		?> 

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>

  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>