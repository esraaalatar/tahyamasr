<?php 
session_start();
?>

<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
	<?php 
include_once "header.php";
	?>
    <!-- END nav -->
    <section class="ftco-section ftco-cart">
			<div class="container">
			<div class="row justify-content-center">
				<?php
  
  include_once "Database.php";
  $db=new Database();
  $rows=$db->RUNSearch("select count(*) from orders where username='".$_SESSION['UserCart']."'");
if($rs=mysqli_fetch_assoc($rows))
{
	$_SESSION['UserOrder']=$rs['count(*)'];
}
 if($_SESSION['UserOrder']>0){
$rows=$db->RUNSearch("select * from orders where username='".$_SESSION['UserCart']."'");
while($rs=mysqli_fetch_assoc($rows))
{
 ?>
    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr class="text-center">
						        <th>&nbsp;</th>
						        <th>#</th>
						        <th>Status</th>
						        <th>Total</th>
						        <th>order date</th>
						      </tr>
						    </thead>
						
						    <tbody>
						      <tr class="text-center">
                  <td class="product-remove"><a href="orderdetails.php?id=<?php echo($rs['order_id']); ?>"><span > show</span></a></td>

						        <td class="product-name">
						        	<h3><?php echo($rs['order_id']); ?></h3>
						        </td>
						        
						        <td class="price"><?php echo($rs['order_status']); ?></td>
						        
						       
									<td class="quantity">$<?php echo($rs['total']); ?></td>
					          		
					        
						        
						        <td class="total"><?php echo($rs['order_date']); ?></td>
						      </tr><!-- END TR-->
							</tbody>
							
						  </table>
					  </div>
					  <?php 
											} }	else echo('<h3> there is no orders <a href="shop.php"> shop now >> </a>  </h3> ');
						   ?>
    			</div>
        </div>
        </section>
<?php
    include_once "footer.php";
		?> 
<!-- footer end -->
  
<script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>