
<?php
  include "Customers.php";

  session_start();
  if(isset($_COOKIE['usercookie'])){
    header('location:index.php');
    }

  if(isset($_POST['btnlogin']))
  {
     $user=new Customers();
      
     $user->setUsername($_POST['username']);
     $user->setPassword($_POST['password']);
     $user->setCustomerPhone($_POST['username']);
      
     
        $r= $user->Login();
       
        if($rows=mysqli_fetch_assoc($r))
           {
             if(isset($_POST['remember']))
             {
              setcookie("usercookie",$_POST['username'],time()+60*60*24*365);
             }
             $_SESSION['Users']=$_POST['username'];
             $_SESSION['UserCart']=$rows['username'];
             if (isset($_GET['redirect'])) {
              header('Location: ' . $_GET['redirect']);
          } else {
            header('location:index.php');
          }
            
           }
        else
        echo "<script type='text/javascript'>alert('Invaild Username or Password');</script>";
 

  }
        

?>
<html lang="en">
  <head>
    <title>Tahya Masr</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
	<?php 
	
include_once "header.php";
	?>
    <!-- END nav -->
	  
<!-- 
    <section class="ftco-section"> -->
    <form method="post" class="form"> 
      <div class="container">
        <div class="row justify-content-center">
       
					<div class="col-xl-5">
	          <div class="row mt-5 pt-3">
	          	<div class="col-md-12 d-flex mb-5">
	          		
              </div>
              
	          	<div class="col-md-12">
	          		<div class="cart-detail p-3 p-md-4">
          
	          			<h3 class="billing-heading mb-4">Login</h3>
									<div class="form-group">
										<div class="col-md-12">
														<div class="form-group">
															<label for="firstname">UserName</label>
														  <input type="text" class="form-control" placeholder="" name="username">
														</div>
													 
														<div class="form-group">
															<label for="lastname">Password</label>
														  <input type="password" class="form-control" placeholder="" name="password">
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" name="remember" class="form-check-input">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="forgetpassword.php" class="auth-link text-black">Forgot password?</a>
                </div>
                            <!-- <a href="forgetpassword.php"> forget password </a>
														<div class="form-group">
														  <input type="checkbox"  name="remember" id="rem" >
                              <label for="remember"> Remember Me </label> -->
                            
                              
                            </div>
                       
                    </div>
                    
         
           <input type="submit" class=" btn btn-danger btn-md form-control " value="Login" name="btnlogin">
           <div class="text-center mt-4 font-weight-light">
                  Don't have an account? <a href="registeration.php" class="text-primary">Create</a>
                </div>
									</div>
                </div>
                         

	          	</div>
            </div>
          </div> <!-- .col-md-8 -->
          
        </div>
      </div>
     
</form>
    <!-- </section> -->
     <!-- .section -->

    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>

  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>