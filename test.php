<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
  <?php 
	include_once "header.php";
		?>  
    <!-- END nav -->



    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
						<form method="post" class="billing-form">
							<h3 class="mb-4 billing-heading">Shipping Details</h3>
	          	<div class="row align-items-end">
				  <?php
				  include_once "Customers.php";
      $users=new Customers();
	  $users->setUsername($_SESSION['Users']); 
	  $users->setCustomerPhone($_SESSION['Users']);
	  $users->setCustomerEmail($_SESSION['Users']);
	  $r=$users->CheckUser();
      if($rows=mysqli_fetch_assoc($r))
           {
          // echo($rows);
               ?>
	          		<div class="col-md-6">
	                <div class="form-group">
					<label for="firstname">Full Name</label>
					<input type="text" class="form-control" name="txtname" value="<?php echo $rows['customer_name']; ?>" required >
	                </div>
	              </div>
              
					<div class="w-100"></div>
				  <div class="col-md-6">
	                <div class="form-group">
	                	<label for="phone">Phone*</label>
	                  <input type="text" class="form-control" name="txtphone" value="<?php echo $rows['customer_phone']; ?>" required >
	                </div>
				  </div>
				  
				  <div class="col-md-6">
	                <div class="form-group">
	                	<label for="landphone">Land Phone</label>
	                  <input type="text" class="form-control" value="<?php echo $rows['customer_land_phone']; ?>" name="landphone" >
	                </div>
	              </div>
                <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
		            		<label for="country">Town / City</label>
		            		<input type="text" class="form-control" value="<?php echo $rows['city']; ?>" name="city" required>
		            	</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="streetaddress">Area</label>
	                  <input type="text" class="form-control"  value="<?php echo $rows['area']; ?>" name="area" required>
	                </div>
		            </div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="towncity">Street</label>
	                  <input type="text" class="form-control" name="street"  value="<?php echo $rows['street']; ?>" required>
	                </div>
		            </div>
		   <?php } ?>
					</div>
	         <!-- END -->
					</div>
					<div class="col-xl-5">
	          <div class="row mt-5 pt-3">
			  <?php
  include_once "Database.php";
  $db=new Database();
  $r=$db->RUNSearch("select sum(subtotal) from cardorders where username='".$_SESSION['UserCart']."'");
	if($row=mysqli_fetch_assoc($r)){
 ?>
	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3 class="billing-heading mb-4">Cart Total</h3>
	          			<p class="d-flex">
		    						<span>Subtotal</span>
		    						<span>$<?php echo($row['sum(subtotal)']); ?></span>
		    					</p>
								<p class="d-flex">
    						<span>Delivery</span>
							<?php $delivery=30.00;?>
    						<span>$	<?php echo($delivery);?>.00</span> 
    					</p>
		    					<p class="d-flex">
		    						<span>Discount</span>
		    						<span>$0.00</span>
		    					</p>
		    					<hr>
		    					<p class="d-flex total-price">
		    						<span>Total</span>
		    						<span>$<?php echo($row['sum(subtotal)']+$delivery); ?>.00</span>
		    					</p>
								
								<div class="form-group">
										<div class="col-md-12">
											<div class="checkbox">
											   <label><input type="checkbox" value="" class="mr-2" checked required> I have read and accept the terms and conditions</label>
											</div>
										</div>
									</div>
									<p> <input type="submit" class="btn btn-primary py-3 px-4" value='Place an order' name="btnorder"></p>
								</div>
								<?php
						
								if(isset($_POST['btnorder']))
								{
									$db=new Database();
									$date=new DateTime("now", new DateTimeZone('Africa/Cairo'));
									$cd=$date->format('Y-m-d H:i:s');
									$total=$row['sum(subtotal)']+$delivery;
									$subtotal=$row['sum(subtotal)'];
									$db->RUNDML("insert into `orders`( `order_date`, `order_status`, `username`, `subtotal`, `delivary`, `total`, `city`, `name`, `phone`, `area`, `street`) VALUES ('".$cd."','pending','".$_SESSION['UserCart']."','".$subtotal."','".$delivery."','".$total."','".$_POST['city']."','".$_POST['txtname']."','".$_POST['txtphone']."','".$_POST['area']."','".$_POST['street']."')","added");
							// $db->RUNDML("insert into `orders`( `order_date`, `order_status`, `username`, `subtotal`, `delivary`, `total`, `city`, `name`, `phone`, `area`, `street`) VALUES ('".$cd."','pending','".$_SESSION['UserCart']."','".$subtotal."','".$delivery."','".$total."','".$_POST['city']."','".$_POST['txtname']."','".$_POST['txtphone']."','".$_POST['area']."','".$_POST['street']."')","added");
									  
								}
						?>
						<?php
					}
					?>
	          	</div>
	    
	          	</div>
	          </div>
          </div> <!-- .col-md-8 -->
        </div>
      </div>
	  </form>
    </section> <!-- .section -->



  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>

    
  </body>
</html>