<?php

include_once "Database.php";
include_once "Operation.php";
class Customers extends Database implements Operation{
  
    var $customerID,$customerName,$customerPhone,$customerLandPhone,$city,$area,$street,$username,$password,$email;
  
    public function getCustomerEmail()
    {
        return $this->email;
    }
    public function setCustomerEmail($Id)
    {
        $this->email=$Id;
    }

    public function getCustomerID()
    {
        return $this->customerID;
    }
    public function setCustomerID($Id)
    {
        $this->customerID=$Id;
    }
    public function getCustomerName()
    {
        return $this->customerName;
    }
    public function setCustomerName($Id)
    {
        $this->customerName=$Id;
    }
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }
    public function setCustomerPhone($Id)
    {
        $this->customerPhone=$Id;
    }
    public function getCustomerLandPhone()
    {
        return $this->customerLandPhone;
    }
    public function setCustomerLandPhone($Id)
    {
        $this->customerLandPhone=$Id;
    }
    public function getCity()
    {
        return $this->city;
    }
    public function setCity($Id)
    {
        $this->city=$Id;
    }
    public function getArea()
    {
        return $this->area;
    }
    public function setArea($Id)
    {
        $this->area=$Id;
    }
    public function getStreet()
    {
        return $this->street;
    }
    public function setStreet($Id)
    {
        $this->street=$Id;
    }
    public function getUsername()
    {
        return $this->username;
    }
    public function setUsername($Id)
    {
        $this->username=$Id;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($Id)
    {
        $this->password=$Id;
    }
    
        public function Add()
        {
            return parent::RUNDML("insert into customers (customer_name,password,username,customer_phone,customer_land_phone,city,area,street,email) values ('".$this->getCustomerName()."','".$this->getPassword()."','".$this->getUserName()."','".$this->getCustomerPhone()."','".$this->getCustomerLandPhone()."','".$this->getCity()."','".$this->getArea()."','".$this->getStreet()."','".$this->getCustomerEmail()."')","User has been created successfully");
        }   
        public function Update()
        {
          return parent::RUNDML("update customers set password='".$this->getPassword()."',customer_name='".$this->getCustomerName()."',customer_phone='".$this->getCustomerPhone()."',city='".$this->getCity()."',customer_land_phone='".$this->getCustomerLandPhone()."',area='".$this->getArea()."',street='".$this->getStreet()."' where username='".$this->getUsername()."'","Profile has been Updated");
          }
    public function Delete(){

    }

    public function Search()
    {

    }

    
  public function Login()
  {
      $log=parent::RunSearch("Select * From customers where (username='".$this->getUsername()."' or  customer_phone='".$this->getCustomerPhone()."') and password='".$this->getPassword()."'");
    return $log;

  }
  
 public function CheckUser()
 {
     $log=parent::RunSearch("Select * From customers where (username='".$this->getUsername()."' or email='".$this->getCustomerEmail()."' or customer_phone='".$this->getCustomerPhone()."')");
   return $log;

 }
 public function UpdatePW()
 {
       return parent::RUNDML("update customers set password='".$this->getPassword()."' where (username='".$this->getUsername()."' or email='".$this->getCustomerEmail()."' or customer_phone='".$this->getCustomerPhone()."')","Password has been updated");
 }


}

?>