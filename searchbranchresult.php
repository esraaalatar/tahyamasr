<?php 
session_start();
?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
	<?php 
include_once "header.php";
	?>
    <!-- END nav -->

<br>
    <section class="ftco-section">
	<form method="post" class="form"> 
			<div class="container">
				<div class="row no-gutters ftco-services">
				<h1> Find Your Branch here </h1>
				</div> 
				<div class="row no-gutters ftco-services">
          <div class="col-md-12 text-center d-flex align-self-stretch ftco-animate ">
          <select name="vendorId" class="form-control" > 
          <?php
                include_once "Branch.php";  
                $dept=new Branch();
                $drows=$dept->GetAllBranches();
                while($rs=mysqli_fetch_assoc($drows))
                {
              ?>
          <option>
          <?php echo($rs['vendor_name']); ?>
           </option> <?php } ?>
                </select>
				 <input type="text" class="form-control" placeholder="City" name="city">
				 <input type="submit" class=" btn btn-danger btn-lg form-control" value="Find" name="btnsearch">
				 </div>
            </div>    
          </div>
		  </form>
		</section>

		<section class="ftco-section ftco-category ftco-no-pt">
			<div class="container">
							<div class="row justify-content-around flex-wrap ">
              <table class="table">
  <thead>
<?php
$rows=$dept->getCityBranches($_GET['city'], $_GET['vno']); 
while($rs=mysqli_fetch_assoc($rows))
{
?>
 
    <tr>
      <th scope="col">#</th>
      <th scope="col">Vendor</th>
      <th scope="col">City</th>
      <th scope="col">Area</th>
      <th scope="col">Phone</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><?php echo($rs['branch_id']); ?></th>
      <td><?php echo($rs['vendor_name']); ?></td>
      <td><?php echo($rs['city']); ?></td>
      <td><?php echo($rs['area']); ?></td>
      <td><?php echo($rs['phone']); ?></td>
    </tr>
          <?php   }?>
                </tbody>
</table>  
                </div>
                
							</div>
		</section>
<?php
    include_once "footer.php";
		?> 
<!-- footer end -->
  
  </body>
</html>