<?php 
session_start();
?>

<!DOCTYPE html>

<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
	<?php 
include_once "header.php";
	?>
    <!-- END nav -->

<br>
		<section class="ftco-section ftco-category ftco-no-pt">
			<div class="container">
							<div class="row justify-content-around flex-wrap ">
              <table class="table">
  <thead>
<?php
include_once "Database.php";
$db=new Database();
$rows=$db->RUNSearch("select * from orderproductdetails where order_id='".$_GET['id']."'");
while($rs=mysqli_fetch_assoc($rows))
{
?>
 
    <tr>
      <th scope="col">#</th>
      <th scope="col">Product name</th>
      <th scope="col">Quantity</th>
      <th scope="col">price</th>
      <th scope="col">total</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    <td><?php echo($rs['product_id']); ?></td>
      <td><?php echo($rs['product_name']); ?></td>
      <td><?php echo($rs['quantity']); ?></td>
      <td><?php echo($rs['price']); ?></td>
      <td><?php echo($rs['total']); ?></td>
    </tr>
          <?php   }?>
      
                </tbody>
</table>  
                </div>
                
							</div>
		</section>

<?php
    include_once "footer.php";
		?> 
<!-- footer end -->
  
  </body>
</html>