<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/fily/template/demo/vertical-default-light/pages/samples/orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 16 Sep 2019 13:47:02 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Fily Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
<?php 
	
  include_once "header.php";
    ?>
          <!-- partial -->
      <div class="main-panel">          
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Products</h4>
                  
                  <div class="row">
                    <div class="col-12">
                      <div class="table-responsive">
                      <form class="forms-sample">
                    <div class="form-group">
                      <label for="exampleInputName1">Name</label>
                      <input type="text" class="form-control" id="exampleInputName1" placeholder="Name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">Price</label>
                      <input type="number" class="form-control" id="exampleInputEmail3" placeholder="Price">
                    </div>
                   
                    <div class="form-group">
                      <label for="exampleSelectVendor">Vendor</label>
                        <select class="form-control" id="exampleSelectVendor">
                        <?php
              
               include_once "Database.php";
               $db=new Database();
               $rows=$db->RUNSearch("select * from vendors");
                while($rs=mysqli_fetch_assoc($rows))
                {
              ?>    
                          <option><?php echo($rs['vendor_name']); ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    
                      <div class="form-group">
                      <label for="exampleSelectGender">Category</label>
                        <select class="form-control" id="exampleSelectGender">
                         <?php
                include_once "../Category.php";  
                $dept=new Category();
                $drows=$dept->GetAllCategories();
                while($rs=mysqli_fetch_assoc($drows))
                {
              ?>                <option> <?php echo($rs['category_name']); ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    <div class="form-group">
                      <label>Image upload</label>
                      <input type="file" name="img[]" class="file-upload-default">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                      </div>
                    </div>
                    
                   
                    <div class="form-group">
                      <label for="exampleTextarea1">Description</label>
                      <textarea class="form-control" id="exampleTextarea1" rows="4"  placeholder="PUT PRODUCT Description HERE"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="vendors/typeahead.js/typeahead.bundle.min.js"></script>
  <script src="/vendors/select2/select2.min.js"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <script src="js/settings.js"></script>
  <script src="js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/file-upload.js"></script>
  <script src="js/typeahead.js"></script>
  <script src="js/select2.js"></script>
  <!-- End custom js for this page-->
  
</body>


<!-- Mirrored from www.urbanui.com/fily/template/demo/vertical-default-light/pages/samples/orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 16 Sep 2019 13:47:03 GMT -->
</html>
