<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/fily/template/demo/vertical-default-light/pages/samples/orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 16 Sep 2019 13:47:02 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Fily Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
<?php 
	
  include_once "header.php";
    ?>
          <!-- partial -->
      <div class="main-panel">          
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Orders</h4>
                  
                  <div class="row">
                    <div class="col-12">
                      <div class="table-responsive">
                        <table id="order-listing" class="table">
                          <thead>
                            <tr class="bg-primary text-white">
                                <th>Product #</th>
                                <th>Name</th>
                                <th>Category Name</th>
                                <th>Vendor Name</th>
                                <th>Price</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php
  
                          include_once "Database.php";
                            $db=new Database();
                            $rows=$db->RUNSearch("select * from viewproducts");
                                  while($rs=mysqli_fetch_assoc($rows))
                            { ?>
                            <tr>
                                <td><?php echo($rs['product_id']); ?></td>
                                <td><?php echo($rs['product_name']); ?></td>
                                <td><?php echo($rs['category_name']); ?></td>
                                <td><?php echo($rs['vendor_name']); ?></td>
                                <td>$<?php echo($rs['price']); ?></td>
                                <td><?php echo($rs['descreption']); ?></td>
                                <td>
                                  <a class="btn btn-light"  href="orderdetails.php?id=<?php echo($rs['order_id']); ?>">
                                    <i class="mdi mdi-eye text-primary"></i>View
                            </a>
                                  <a class="btn btn-light" href="deletecartitem.php?id=<?php echo($rs['order_id']); ?>">
                                    <i class="mdi mdi-close text-danger"></i>Remove
                            </a>
                                </td>
                            </tr>
                            <?php 
											}
						   ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2018 <a href="https://www.urbanui.com/" target="_blank">Urbanui</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <script src="js/settings.js"></script>
  <script src="js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/data-table.js"></script>
  <!-- End custom js for this page-->
</body>


<!-- Mirrored from www.urbanui.com/fily/template/demo/vertical-default-light/pages/samples/orders.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 16 Sep 2019 13:47:03 GMT -->
</html>
