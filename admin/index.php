<?php 
session_start();
?>
<html lang="en">
<?php if(isset($_SESSION['Users'])){ ?>
<!-- Mirrored from www.urbanui.com/fily/template/demo/vertical-default-light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 16 Sep 2019 13:43:43 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Fily Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
  
  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="//cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
<?php 
	
include_once "header.php";
	?>
        <!-- partial -->
        <div class="main-panel">
        <div class="content-wrapper">
  <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="card bg-white">
                  <div class="card-body d-flex align-items-center justify-content-between">
                    <h4 class="mt-1 mb-1">Hi, Welcomeback!</h4>
                    <button class="btn btn-info d-none d-md-block">Import</button>
                  </div>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 grid-margin stretch-card">
              <div class="card border-0 border-radius-2 bg-success">
                <div class="card-body">
                  <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
                    <div class="icon-rounded-inverse-success icon-rounded-lg">
                      <i class="mdi mdi-arrow-top-right"></i>
                    </div>
                    <div class="text-white">
                      <p class="font-weight-medium mt-md-2 mt-xl-0 text-md-center text-xl-left">Total Users</p>
                      <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                      <?php
  
  include_once "Database.php";
    $db=new Database();
     $rows=$db->RUNSearch("select count(*) from customers");
          if($rs=mysqli_fetch_assoc($rows))
                            { ?>
                        <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1"><?php echo($rs['count(*)']); ?></h3>
                       
                        <?php 
											}
						   ?>
                        <small class="mb-0">This Year</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 grid-margin stretch-card">
              <div class="card border-0 border-radius-2 bg-info">
                <div class="card-body">
                  <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
                    <div class="icon-rounded-inverse-info icon-rounded-lg">
                      <i class="mdi mdi-basket"></i>
                    </div>
                    <div class="text-white">
                      <p class="font-weight-medium mt-md-2 mt-xl-0 text-md-center text-xl-left">Total Sales</p>
                      <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                      <?php
  
  include_once "Database.php";
    $db=new Database();
     $rows=$db->RUNSearch("select sum(total) from orders");
          if($rs=mysqli_fetch_assoc($rows))
                            { ?>
                        <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1">$<?php echo($rs['sum(total)']); ?></h3>
                        <?php 
											}
						   ?>
                        <small class="mb-0">This month</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 grid-margin stretch-card">
              <div class="card border-0 border-radius-2 bg-danger">
                <div class="card-body">
                  <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
                    <div class="icon-rounded-inverse-danger icon-rounded-lg">
                      <i class="mdi mdi-chart-donut-variant"></i>
                    </div>
                    <div class="text-white">
                      <p class="font-weight-medium mt-md-2 mt-xl-0 text-md-center text-xl-left">Total Orders</p>
                      <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                      <?php
  
        include_once "Database.php";
          $db=new Database();
           $rows=$db->RUNSearch("select count(*) from orders");
                if($rs=mysqli_fetch_assoc($rows))
                                  { ?>
                        <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1"><?php echo($rs['count(*)']); ?></h3>
                        <?php 
											}
						   ?>
                        <small class="mb-0">Order/s</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 grid-margin stretch-card">
              <div class="card border-0 border-radius-2 bg-warning">
                <div class="card-body">
                  <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
                    <div class="icon-rounded-inverse-warning icon-rounded-lg">
                      <i class="mdi mdi-chart-multiline"></i>
                    </div>
                    <div class="text-white">
                      <p class="font-weight-medium mt-md-2 mt-xl-0 text-md-center text-xl-left">Pending Orders</p>
                      <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                      <?php
  
  include_once "Database.php";
    $db=new Database();
     $rows=$db->RUNSearch("select count(*) from orders where order_status='pending'");
          if($rs=mysqli_fetch_assoc($rows))
                            { ?>
                  <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1"><?php echo($rs['count(*)']); ?></h3>
                  <?php 
                }
         ?>
                        <small class="mb-0">Order</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">Total Sales</p>
                  <p class="text-muted">Audience to which the users belonged while on the current date Audience to which the users belonged while on the current date Audience to which the users belonged while on the current date </p>
                  <div class="d-flex flex-wrap mb-4 mt-4 pb-4">
                    <div class="mr-4 mr-md-5">
                      <p class="mb-0">Revenue</p>
                      <h4>13,956</h4>
                    </div>
                    <div class="mr-4 mr-md-5">
                      <p class="mb-0">Returns</p>
                      <h4>27,219</h4>
                    </div>
                    <div class="mr-4 mr-md-5">
                      <p class="mb-0">Queries</p>
                      <h4>03,386</h4>
                    </div>
                    <div class="mr-4 mr-md-5">
                      <p class="mb-0">Invoices</p>
                      <h4>04,739</h4>
                    </div>
                  </div>
                  <canvas id="total-sales-chart"></canvas>
                </div>
              </div>
            </div>
            <div class="col-xl-6 grid-margin">
              <div class="row">
                <div class="col-md-6 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-title">Users</p>
                      <div class="d-flex flex-wrap align-items-baseline">
                          <h2 class="mr-3">33,956</h2>
                          <i class="mdi mdi-arrow-up mr-1 text-danger"></i><span><p class="mb-0 text-danger font-weight-medium">+2.12%</p></span>
                      </div>
                      <p class="mb-0 text-muted">Total users world wide</p>
                    </div>
                    <canvas id="users-chart"></canvas>
                  </div>
                </div>
                <div class="col-md-6 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-title">Projects</p>
                      <div class="d-flex flex-wrap align-items-baseline">
                        <h2 class="mr-3">50.36%</h2>
                        <i class="mdi mdi-arrow-up mr-1 text-success"></i><span><p class="mb-0 text-success font-weight-medium">+9.12%</p></span>                          
                      </div>
                      <p class="mb-0 text-muted">Total users world wide</p>
                    </div>
                    <canvas id="projects-chart"></canvas>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-title">Downloads</p>
                      <p class="text-muted mb-2">Watching ice melt. This is fun. Only you could make those words cute.</p>
                      <div class="row mt-4">
                        <div class="col-md-6 stretch-card">
                          <div class="row d-flex align-items-center">
                            <div class="col-6">
                              <div id="offlineProgress"></div>                              
                            </div>
                            <div class="col-6 pl-0">
                              <p class="mb-0">Offline</p>
                              <h2>45,324</h2>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 stretch-card mt-4 mt-md-0">
                          <div class="row d-flex align-items-center">
                            <div class="col-6">
                              <div id="onlineProgress"></div>                              
                            </div>
                            <div class="col-6 pl-0">
                              <p class="mb-0">Online</p>
                              <h2>12,236</h2>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>  
        
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="vendors/chart.js/Chart.min.js"></script>
  <script src="http://www.urbanui.com/"></script>
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/template.js"></script>
  <script src="js/settings.js"></script>
  <script src="js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

<?php 
 } 
 else {
 header('location:login.php');
 }?>
</html>

