<?php 
session_start();
?>
		<?php 
		// if(isset($_POST['btnadd']))
		// {
		// include_once "Database.php";
		// $db=new Database();
		// $db->RUNDML("insert into add_product Values ('".$_GET['prno']."','".$_POST['quantity']."','".$_SESSION['UserCart']."')","");
		// }
		?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
	<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

  </head>
  <body class="goto-here">
  <?php 
	include_once "header.php";
		?>  
    <!-- END nav -->

    <section class="ftco-section">
	<form id='product_form' method=post>
    	<div class="container">
    		<div class="row">
			<?php
  
  include_once "Products.php";
  $pro=new Products();
  $productId=$_GET['prno'];
  $rs=$pro->SearchByID($productId);
  if($row=mysqli_fetch_assoc($rs))
  {
    ?>
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="images/product-<?php echo($row['product_id']); ?>.jpg" class="image-popup"><img src="images/product-<?php echo($row['product_id']); ?>.jpg" class="img-fluid" alt="Colorlib Template"></a>
    			</div>
				
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
				<span style="display:none;" id='pro_id'><?php echo($row['product_id']); ?></span>
    				<h3><?php echo($row['product_name']); ?></h3>
    				<p class="price"><span>$<?php echo($row['price']); ?></span></p>
    				<p><?php echo($row['descreption']); ?></p>
						<div class="row mt-4">
							<div class="col-md-6">
								<div class="d-flex">
		              <div>
					  Sold by : <?php echo($row['vendor_name']); ?>
	                </div>
		            </div>
							</div>
							<div class="w-100"></div>
							<div class="input-group col-md-6 d-flex mb-3">
	             	<span class="input-group-btn mr-2">
	                	<button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
	                   <i class="ion-ios-remove"></i>
	                	</button>
	            		</span>
	             	<input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
	             	<span class="input-group-btn ml-2">
	                	<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
	                     <i class="ion-ios-add"></i>
	                 </button>
	             	</span>
	          	</div>
	          	<div class="w-100"></div>
	          	<div class="col-md-12">
	          		<p style="color: #000;">600 kg available not working</p>
	          	</div>
          	</div>
          	<p><input type="submit" class="btn btn-black py-3 px-5" value="Add to Cart" name="btnadd"></p>
				</div>
    		</div>
    	</div>
</form>
<script>

var productNO = $('#pro_id').val();
		console.log("productNO: ", $('#pro_id').html());

    $(`#product_form`).on(`submit`, function (e) {
        e.preventDefault();
		
		var productNO = $('#pro_id').html();
		console.log("productNO: ", productNO);
        var quantity = $(`#quantity`).val().trim();
       

        $.ajax(`/esraa/tahyamassr/ajax_request.php`, {
            method: `POST`,
            data: {
				'btnadd': 'btnadd',
				'pro_num' : productNO,
                'quantity': quantity,
            },
            success: function (data, textStatus, jqXHR) {
				pronum = JSON.parse(data);
				console.log("data: ", pronum);
				$(`#p_number`).html(pronum['numbers']);
               
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log("Error: ", errorThrown);
            }
        })

    });

</script>
    	<div class="container">
				<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<span class="subheading">Products</span>
            <h2 class="mb-4">Related Products</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
          </div>
        </div>   		
    	</div>
    	<div class="container">
    		<div class="row">
			<?php
			$rsd=$pro->RelatedPro($row['category_id'],$_GET['prno']);
  while($rows=mysqli_fetch_assoc($rsd))
  {
    ?>
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="product">
    					<a href="productsingle.php?prno=<?php echo($rows['product_id']); ?>" class="img-prod"><img class="img-fluid" src="images/product-<?php echo($rows['product_id']); ?>.jpg" alt="Colorlib Template">
    						<div class="overlay"></div>
    					</a>
    					<div class="text py-3 pb-4 px-3 text-center">
    						<h3><a href="productsingle.php?prno=<?php echo($rows['product_id']); ?>"><?php echo($rows['product_name']); ?></a></h3>
    						<div class="d-flex">
    							<div class="pricing">
		    						<p class="price"><span>$<?php echo($rows['price']); ?></span></p>
		    					</div>
	    					</div>
    						<div class="bottom-area d-flex px-3">
	    						<div class="m-auto d-flex">
	    							<a href="#" class="add-to-cart d-flex justify-content-center align-items-center text-center">
	    								<span><i class="ion-ios-menu"></i></span>
	    							</a>
	    							<a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
	    								<span><i class="ion-ios-cart"></i></span>
	    							</a>
	    							<a href="#" class="heart d-flex justify-content-center align-items-center ">
	    								<span><i class="ion-ios-heart"></i></span>
	    							</a>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
				<?php }} ?>
    		</div>
    	</div>
    </section>

	<?php 
	
	include_once "footer.php";
		?> 
  <!-- footer end-->


  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>