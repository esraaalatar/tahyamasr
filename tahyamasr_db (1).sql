-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2019 at 03:48 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tahyamasr_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_product`
--

CREATE TABLE IF NOT EXISTS `add_product` (
  `product_id` int(11) NOT NULL,
  `quantity` int(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_id` (`product_id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=8 ;

--
-- Dumping data for table `add_product`
--

INSERT INTO `add_product` (`product_id`, `quantity`, `username`, `id`) VALUES
(2, 0, NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `name`, `date_created`, `created_by`) VALUES
(1, 'admin', 'admin', 'admin', '2019-09-18 13:55:55', 'admin'),
(2, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `area` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`branch_id`) USING BTREE,
  KEY `vendor_id` (`vendor_id`) USING BTREE,
  KEY `branch_id` (`branch_id`,`vendor_id`) USING BTREE,
  KEY `branches_ibfk_2` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=5 ;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `vendor_id`, `city_id`, `area`, `phone`, `address`) VALUES
(1, 1, 1, 'nasr city', '02332563255', '1st sstreet'),
(2, 1, 2, 'feisal', '0515151515', '1st golf'),
(3, 2, 2, 'feisal', '05000505555', '2nd golf'),
(4, 2, 1, 'nasr city', '02332652525', '2nd street');

-- --------------------------------------------------------

--
-- Stand-in structure for view `cardorders`
--
CREATE TABLE IF NOT EXISTS `cardorders` (
`vendor_name` varchar(255)
,`quantity` int(255)
,`product_id` int(11)
,`product_name` varchar(255)
,`price` decimal(10,2)
,`subtotal` decimal(65,2)
,`category_id` int(11)
,`category_name` varchar(255)
,`vendor_id` int(11)
,`customer_name` varchar(255)
,`username` varchar(255)
,`id` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=7 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(1, 'Vegetables'),
(2, 'Fruits'),
(3, 'Juices'),
(4, 'Dried'),
(5, 'Meat'),
(6, 'Starches');

-- --------------------------------------------------------

--
-- Stand-in structure for view `categorycityproductsview`
--
CREATE TABLE IF NOT EXISTS `categorycityproductsview` (
`vendor_name` varchar(255)
,`product_id` int(11)
,`product_name` varchar(255)
,`price` decimal(10,2)
,`descreption` varchar(255)
,`category_id` int(11)
,`vendor_id` int(11)
,`area` varchar(255)
,`phone` varchar(255)
,`address` varchar(255)
,`category_name` varchar(255)
,`city` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `city`) VALUES
(1, 'cairo'),
(2, 'giza');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(255) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `customer_phone` varchar(255) NOT NULL,
  `customer_land_phone` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  PRIMARY KEY (`customer_id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `customer_phone` (`customer_phone`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=14 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `username`, `password`, `email`, `customer_phone`, `customer_land_phone`, `city`, `area`, `street`) VALUES
(1, 'sd', 'sd', 'esraa3', '', 'sd', 'sd', '', '', ''),
(2, 'aa', 'aa', 'esraa3', '', 'aa', 'aa', '', '', ''),
(6, 'esraa', 'esraaalatar', 'esraa', 'esraaalatar@gmail.com', '01158130846', '', '', '', ''),
(7, 'ahmed', 'ahmed', 'ahmed', 'ahmed@gmail.com', '0122154151', '', '', '', ''),
(8, 'khaled essam', 'kessam', '', 'khaled@gmail.com', '01221542151', '', '', 'mokattam', '1205 '),
(12, 'esraa', 'esraa', 'esraa', 'esraaalatar@gmail.com', '011581308466', '', 'cairo', 'mokattam', ''),
(13, 'sarah', 'sara', 'sara', 'sara@gmail.com', '0023402340', '0238238', 'cairo', 'feisal', 'feisal');

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `delivery_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `deliver_date` datetime DEFAULT NULL,
  PRIMARY KEY (`delivery_id`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `delivery_ibfk_2` (`vendor_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `delivery_captians`
--

CREATE TABLE IF NOT EXISTS `delivery_captians` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `delivery_captians`
--

INSERT INTO `delivery_captians` (`id`, `name`, `password`, `phone`, `city`, `status`) VALUES
(1, 'ahmed', 'ahmed', '026262626', 'cairo', 'busy'),
(2, 'tarek', 'tarek', '03155151', 'giza', 'available'),
(3, 'mostafa', 'mostafa', '0255251', 'cairo', 'busy'),
(4, 'khaled', 'khaled', '06255156', 'giza', 'available');

-- --------------------------------------------------------

--
-- Stand-in structure for view `orderproductdetails`
--
CREATE TABLE IF NOT EXISTS `orderproductdetails` (
`price` float(10,2)
,`quantity` float(255,0)
,`total` float(255,0)
,`product_name` varchar(255)
,`order_id` int(11)
,`product_id` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` datetime DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `shipped_date` datetime DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `subtotal` float(255,0) DEFAULT NULL,
  `delivary` float(255,0) DEFAULT NULL,
  `total` float(255,0) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`) USING BTREE,
  KEY `delivery_id` (`delivery_id`) USING BTREE,
  KEY `orders_ibfk_3` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=21 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_date`, `order_status`, `shipped_date`, `comments`, `delivery_id`, `username`, `subtotal`, `delivary`, `total`, `city`, `name`, `phone`, `area`, `street`) VALUES
(20, '2019-09-16 15:18:55', 'pending', NULL, NULL, 3, 'esraa', 12, 30, 42, 'cairo', 'esraa', '011581308466', 'mokattam', 'esraa');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` float(255,0) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `total` float(255,0) DEFAULT NULL,
  PRIMARY KEY (`order_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`order_id`, `product_id`, `quantity`, `price`, `total`) VALUES
(20, 10, 1, 12.00, 12);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `descreption` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE,
  KEY `vendor_id` (`vendor_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=13 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `price`, `descreption`, `category_id`, `vendor_id`) VALUES
(1, 'p1', '20.00', NULL, 1, 1),
(2, 'p2', '12.00', NULL, 6, 2),
(3, 'p3', '12.00', NULL, 2, 1),
(4, 'p4', '12.00', NULL, 3, 2),
(5, 'p5', '12.00', NULL, 5, 1),
(6, 'p6', '12.00', NULL, 4, 2),
(7, 'p7', '12.00', NULL, 5, 1),
(8, 'p8', '12.00', NULL, 6, 1),
(9, 'p8', '12.00', NULL, 1, 1),
(10, 'p10', '12.00', NULL, 2, 2),
(11, 'p11', '12.00', NULL, 3, 1),
(12, 'p12', '12.00', NULL, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock_availability`
--

CREATE TABLE IF NOT EXISTS `stock_availability` (
  `branch_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stock` int(255) DEFAULT NULL,
  PRIMARY KEY (`branch_id`,`vendor_id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Stand-in structure for view `vendorbranchview`
--
CREATE TABLE IF NOT EXISTS `vendorbranchview` (
`vendor_name` varchar(255)
,`branch_id` int(11)
,`area` varchar(255)
,`address` varchar(255)
,`phone` varchar(255)
,`vendor_id` int(11)
,`city_id` int(11)
,`city` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vendor_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`vendor_id`, `vendor_name`, `username`, `password`) VALUES
(1, 'tahya masr', 'gesh', 'gesh'),
(2, 'Ministry Of Agriculture', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `viewproducts`
--
CREATE TABLE IF NOT EXISTS `viewproducts` (
`product_id` int(11)
,`product_name` varchar(255)
,`price` decimal(10,2)
,`descreption` varchar(255)
,`category_id` int(11)
,`vendor_id` int(11)
,`category_name` varchar(255)
,`vendor_name` varchar(255)
);
-- --------------------------------------------------------

--
-- Structure for view `cardorders`
--
DROP TABLE IF EXISTS `cardorders`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cardorders` AS select `vendors`.`vendor_name` AS `vendor_name`,`add_product`.`quantity` AS `quantity`,`products`.`product_id` AS `product_id`,`products`.`product_name` AS `product_name`,`products`.`price` AS `price`,(`products`.`price` * `add_product`.`quantity`) AS `subtotal`,`products`.`category_id` AS `category_id`,`categories`.`category_name` AS `category_name`,`products`.`vendor_id` AS `vendor_id`,`customers`.`customer_name` AS `customer_name`,`add_product`.`username` AS `username`,`add_product`.`id` AS `id` from ((((`products` join `add_product` on((`add_product`.`product_id` = `products`.`product_id`))) join `customers` on((`add_product`.`username` = `customers`.`username`))) join `vendors` on((`products`.`vendor_id` = `vendors`.`vendor_id`))) join `categories` on((`products`.`category_id` = `categories`.`category_id`)));

-- --------------------------------------------------------

--
-- Structure for view `categorycityproductsview`
--
DROP TABLE IF EXISTS `categorycityproductsview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `categorycityproductsview` AS select `vendors`.`vendor_name` AS `vendor_name`,`products`.`product_id` AS `product_id`,`products`.`product_name` AS `product_name`,`products`.`price` AS `price`,`products`.`descreption` AS `descreption`,`products`.`category_id` AS `category_id`,`products`.`vendor_id` AS `vendor_id`,`branches`.`area` AS `area`,`branches`.`phone` AS `phone`,`branches`.`address` AS `address`,`categories`.`category_name` AS `category_name`,`cities`.`city` AS `city` from ((((`vendors` join `products` on((`products`.`vendor_id` = `vendors`.`vendor_id`))) join `branches` on((`branches`.`vendor_id` = `vendors`.`vendor_id`))) join `categories` on((`products`.`category_id` = `categories`.`category_id`))) join `cities` on((`branches`.`city_id` = `cities`.`city_id`)));

-- --------------------------------------------------------

--
-- Structure for view `orderproductdetails`
--
DROP TABLE IF EXISTS `orderproductdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orderproductdetails` AS select `order_details`.`price` AS `price`,`order_details`.`quantity` AS `quantity`,`order_details`.`total` AS `total`,`products`.`product_name` AS `product_name`,`order_details`.`order_id` AS `order_id`,`order_details`.`product_id` AS `product_id` from (`products` join `order_details` on((`order_details`.`product_id` = `products`.`product_id`)));

-- --------------------------------------------------------

--
-- Structure for view `vendorbranchview`
--
DROP TABLE IF EXISTS `vendorbranchview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vendorbranchview` AS select `vendors`.`vendor_name` AS `vendor_name`,`branches`.`branch_id` AS `branch_id`,`branches`.`area` AS `area`,`branches`.`address` AS `address`,`branches`.`phone` AS `phone`,`vendors`.`vendor_id` AS `vendor_id`,`branches`.`city_id` AS `city_id`,`cities`.`city` AS `city` from ((`branches` join `vendors` on((`branches`.`vendor_id` = `vendors`.`vendor_id`))) join `cities` on((`branches`.`city_id` = `cities`.`city_id`)));

-- --------------------------------------------------------

--
-- Structure for view `viewproducts`
--
DROP TABLE IF EXISTS `viewproducts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `viewproducts` AS select `products`.`product_id` AS `product_id`,`products`.`product_name` AS `product_name`,`products`.`price` AS `price`,`products`.`descreption` AS `descreption`,`products`.`category_id` AS `category_id`,`products`.`vendor_id` AS `vendor_id`,`categories`.`category_name` AS `category_name`,`vendors`.`vendor_name` AS `vendor_name` from ((`products` join `vendors` on((`products`.`vendor_id` = `vendors`.`vendor_id`))) join `categories` on((`products`.`category_id` = `categories`.`category_id`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `add_product`
--
ALTER TABLE `add_product`
  ADD CONSTRAINT `add_product_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `add_product_ibfk_4` FOREIGN KEY (`username`) REFERENCES `customers` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `branches_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`);

--
-- Constraints for table `delivery`
--
ALTER TABLE `delivery`
  ADD CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `delivery_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`delivery_id`) REFERENCES `delivery_captians` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`username`) REFERENCES `customers` (`username`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stock_availability`
--
ALTER TABLE `stock_availability`
  ADD CONSTRAINT `stock_availability_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
